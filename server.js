//import http
const http = require('http');

//import app file
const app = require('./app');

//define server port
const port = process.env.PORT || 3000;

//create the server
const server = http.createServer(app);

//start the server
server.listen(port);
