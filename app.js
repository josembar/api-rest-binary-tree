//import express
const express = require('express');

const app = express();

//import binary-trees file
const binaryTrees = require('./api/routes/binary-trees');

app.use('/binary-trees',binaryTrees);

module.exports = app;