This application creates a binary tree given a name and a root (initial value) and finds the lowest common ancestor of 
the binary tree given two values.

To create a binary tree go to the next URL:
https://rest-api-binary-tree.herokuapp.com/binary-trees/create-tree/name={name}&root={root}

{name} must be replaced by the name you desire and {root} by a number

To obtain the binary tree you created go to the next URL
https://rest-api-binary-tree.herokuapp.com/binary-trees/get-tree/name={name}

{name} must be replaced by the name of the binary tree you created

To insert a value in an existing binary tree, go to the next URL:
https://rest-api-binary-tree.herokuapp.com/binary-trees/insert-value/name={name}&value={value}

{name} must be replaced by the name of the binary tree you created and {value} by a number you wish to insert into the binary tree

Finally, to get the lowest common ancestor given two values, go to this URL:
https://rest-api-binary-tree.herokuapp.com/binary-trees/lowest-ancestor/name={name}&value1={value1}&value2={value2}

{name} must be replaced by the name of the binary tree you created, {value1} and {value2} by the two numbers you wish to evaluate

