//import express
const express = require('express');

//create a router
const router = express.Router();

let newBinaryTree = {name: null, binaryTree: null};

//obtain a binary tree that was create before looking up by its name
router.get('/get-tree/name=:name',(req,res,next) => {
    const name = req.params.name
    if(name == newBinaryTree.name)
    {
        res.status(200).json({
            BinaryTree: newBinaryTree
        });
    }
});

//obtain the lowest ancestor given a binary tree and two values
router.get('/lowest-ancestor/name=:name&value1=:value1&value2=:value2',(req,res,next) => {
    const name = req.params.name
    const value1 = req.params.value1
    const value2 = req.params.value2
    if(name == newBinaryTree.name)
    {
        if(value1 && value2)
        {
            res.status(200).json({
                BinaryTree: newBinaryTree.name,
                LowestCommonAncestor: findLowestCommonAncestor(newBinaryTree.binaryTree,value1,value2)
            });
        }
    }
});

//create a new binary tree given a name an a root
router.get('/create-tree/name=:name&root=:value',(req,res,next) => {
    const name = req.params.name
    const number = req.params.value;
    if(name)
    {
        newBinaryTree.name = name
        if(number)
        {
            newBinaryTree.binaryTree = new BinaryTree(number)
            res.status(200).json({
                BinaryTree: newBinaryTree
            });
        }
    }
    
});

//insert a value in a binary tree given its name and a value
router.get('/insert-value/name=:name&value=:value',(req,res,next) => {
    const name = req.params.name
    const number = req.params.value;
    if(name == newBinaryTree.name)
    {
        if(number)
        {
            newBinaryTree.binaryTree.insert(number)
            res.status(200).json({
                BinaryTree: newBinaryTree
            });
        }
    }
});

module.exports = router;

//binary tree class
class BinaryTree 
{
    constructor(val) 
    {
        this.value = val;
        this.left = null;
        this.right = null;
    }

    //this function inserts new number in a binary array
    insert(val) 
    {
        if (val < this.value && this.left) 
        {
            this.left.insert(val);
        }
        if (val < this.value && !this.left) 
        {
            this.left = new BinaryTree(val);
        }
        if (val > this.value && this.right) 
        {
            this.right.insert(val);
        }
        if (val > this.value && !this.right) 
        {
            this.right = new BinaryTree(val);
        }
    }
}

//this function finds the lowest common ancestor given two nodes of the tree
function findLowestCommonAncestor(bTree,val1,val2) 
{
    if (bTree.value > val1 && bTree.value > val2) 
    {
        return findLowestCommonAncestor(bTree.left, val1, val2);
    } 
    
    else if (bTree.value < val1 && bTree.value < val2) 
    {
        return findLowestCommonAncestor(bTree.right, val1, val2);
    } 

    else 
    {
        return bTree.value;
    }
}


